const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET =process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user: 'root',
    password: 'root',
    database: 'HotelSystem'
});

connection.connect()

const express = require('express')
const app = express()
const port = 4000

/*Middleware for Athenticating User Token*/
function authenticateToken(req,res,next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET,(err,user)=>{
        if(err){ return res.sendStatus(403)}
        else{
            req.user = user
            next()
        }
    })
}

app.post("/register",(req,res)=>{
    let customer_name=req.query.customer_name
    let customer_surname=req.query.customer_surname
    let customer_sex=req.query.customer_sex
    let customer_age=req.query.customer_age
    let customer_username =req.query.customer_username
    let customer_password =req.query.customer_password



    bcrypt.hash(customer_password, SALT_ROUNDS, (err, hash) =>{

    
        let query = `INSERT INTO Customer
    (Name, Surname, Sex, Age, Username, Password)
    value ('${customer_name}','${customer_surname}','${customer_sex}',
    ${customer_age},'${customer_username}','${hash}')`

    console.log(query);
    connection.query( query,(err,rows)=>{
    if(err){  
        console.log(err);  
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
    
            })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding new user succesful"
            })
        }
         })
    })
})

app.post("/register_hotel", authenticateToken, (req,res)=>{
    let user_profile = req.user
    
    let customer_id = req.user.user_id
    let location_id = req.query.location_id
    let amount = req.query.amount
    let room = req.query.room


    let query = `INSERT INTO Register 
                (CustomerID, LocationID, Room, Amount, DateTime) 
                value ('${customer_id}' ,
                        '${location_id}',
                        '${room}','${amount}',
                        NOW() )`

    console.log(query);

    connection.query( query,(err,rows)=>{
    if(err){
        res.json({
                    "status" : "400",
                    "message" : "Error inserting data into db"

        })
    }else{
        res.json({
                    "status" : "200",
                    "message" : "Adding event succesful"
        })
    }
     })
})

app.get("/list_reg_hotel", authenticateToken, (req,res)=>{
    let user_profile = req.user

    let customer_id = user_profile.user_id;
    
    
    let query=`
                SELECT  Customer.CustomerID,
                        Customer.Name,Customer.Surname,
                        Register.Room,Register.DateTime 

                FROM Customer,Register,LocationHotel 

                WHERE   (Register.CustomerID = Customer.CustomerID) AND
                        (Register.LocationID = LocationHotel.HotelID) AND 
                        (Customer.CustomerID = 1);`

    connection.query( query,(err,rows)=>{
        if(err){
            console.log(err);
            res.json({
                        "status" : "400",
                        "message" : "Error query from running db"

            })
        }else{
            res.json(rows)
        };
    })
    
})

app.get("/list_location",(req,res)=>{
    query="SELECT * from LocationHotel";
    connection.query( query,(err,rows)=>{
        if(err){
            res.json({
                        "status" : "400",
                        "message" : "Error query from running db"

            })
        }else{
            res.json(rows)
        }
    })
})


app.post("/add_location", (req, res)  =>  {

    let location_name = req.query.location_name
    let location_provice = req.query.location_provice
    let location_leval = req.query.location_leval

    let query = 
    `INSERT INTO LocationHotel
        (NameHotel,Province, Leval)
        value ('${location_name}',
                '${location_provice}',
                '${location_leval}')`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }
    });
})

app.post("/update_location", (req, res)  =>  {

    let location_id = req.query.location_id
    let location_name = req.query.location_name
    let location_provice = req.query.location_provice
    let location_leval = req.query.location_leval

    let query = `UPDATE LocationHotel SET
                NameHotel = '${location_name}',
                Province ='${location_provice}',
                Leval = '${location_leval}'
            WHERE HotelID =  ${location_id}`

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error updating data record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating event succesful"
            })
        }
    });
})

app.post("/delete_location", (req, res)  =>  {

    let location_id = req.query.location_id

    let query =` DELETE FROM LocationHotel WHERE HotelID =  ${location_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                 })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting event succesful"
         })
      }
    });
})

app.post("/login",(req,res)=> {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Customer WHERE Username='${username}'`
    connection.query( query,(err,rows)=>{
        if(err){
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error query from running db"

            })
        }else{
            let db_password=rows[0].Password
            bcrypt.compare(user_password, db_password,(err,result)=>{
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].CustomerID,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                    res.send(token)
                }else{ (res.send("Invalid username /password")) }
            })
        }
    })
})
/*token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBpeWF
3YW4iLCJ1c2VyX2lkIjoxLCJpYXQiOjE2NDc2OTI0OTUsImV4cCI6MTY0Nzc3ODg5NX0.
91cb6ebOM6K15BJiDTIcwOdk00ACeNnqQQ_7-ejllM0*/


app.listen(port,()=>{
    console.log(`Now starting Running System Backend ${port}`);
});